//
//  UsernameViewController.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit
import Cartography
import Hero
import MaterialComponents.MaterialTextFields
import MaterialComponents.MaterialButtons
import MaterialComponents.MDCActivityIndicator
import PromiseKit

class UsernameViewController: UIViewController {
  private lazy var usernameTextField: MDCTextField = {
    let textField = MDCTextField()
    textField.placeholder = "Github Username"
    textField.delegate = self
    
    return textField
  }()
  
  private lazy var submitButton: MDCRaisedButton = {
    let button = MDCRaisedButton()
    button.setElevation(ShadowElevation.init(CGFloat(4.0)), for: .normal)
    button.setTitle("search", for: .normal)
    button.sizeToFit()
    button.addTarget(self, action: #selector(search), for: .touchUpInside)
  
    return button
  }()
  
  private lazy var activity: MDCActivityIndicator = {
    let indicator = MDCActivityIndicator()
    indicator.indicatorMode = .indeterminate
    indicator.alpha = 0.0
    
    return indicator
  }()
  
  fileprivate var textFieldControllerFloating: MDCTextInputController?
  private var isLoading = false
  
  class func navWrapped() -> UINavigationController {
    return UINavigationController(rootViewController: UsernameViewController())
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    self.title = "Search Github"
    self.configureViews()
  }
  
  private func configureViews() {
    textFieldControllerFloating = MDCTextInputControllerLegacyDefault(textInput: self.usernameTextField)
    self.view.addSubview(self.usernameTextField)
    self.view.addSubview(self.submitButton)
    self.view.addSubview(self.activity)
    constrain(self.usernameTextField, self.submitButton, self.activity) { (textfield, button, activity) in
      textfield.top == textfield.superview!.top + 60
      textfield.leading == textfield.superview!.leading + 15
      textfield.trailing == textfield.superview!.trailing - 15
      
      button.top == textfield.bottom + 10
      button.leading == textfield.leading
      button.trailing == textfield.trailing
      
      activity.center == activity.superview!.center
      activity.width == activity.height
      activity.width == 50
    }
  }
  
  private func handleError(error:ErrorResponse) {
    self.toggleIndictor()
    self.textFieldControllerFloating?.setErrorText(error.message, errorAccessibilityValue: nil)
  }
  
  @objc private func search() {
    let username = self.usernameTextField.text ?? ""
    self.toggleIndictor()
    
    let userPromise = User.get(username: username)
    let repoPromise = Repo.get(username: username)
    
    when(fulfilled: userPromise, repoPromise).then { u, r -> Void in
      if let error = u.1 ?? r.1 {
        self.handleError(error: error)
      }
      
      if let user = u.0,
         let repos = r.0 {
        self.presentDetail(user: user, repos: repos)
      }
      }.catch { error-> Void in
        self.handleError(error: error as! ErrorResponse)
    }
  }
  
  private func presentDetail(user: User, repos: [Repo]) {
    self.toggleIndictor()
    self.usernameTextField.text = nil
    let userViewController = UserViewController(user: user, repos: repos)
    self.navigationController?.pushViewController(userViewController, animated: true)
  }
  
  private func toggleIndictor() {
    self.isLoading = !self.isLoading
    let alpha = self.isLoading ? CGFloat(1.0):0.0
    self.isLoading ? self.activity.startAnimating():self.activity.stopAnimating()
    
    UIView.animate(withDuration: 0.3) {
      self.activity.alpha = alpha
    }
  }
}

extension UsernameViewController: UITextFieldDelegate {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    self.textFieldControllerFloating?.setErrorText(nil, errorAccessibilityValue: nil)
    
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.search()
    return true
  }
}
