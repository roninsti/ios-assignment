//
//  UserViewController.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit
import Cartography
import MaterialComponents

class UserViewController: UIViewController {
  let user: User
  let repos: [Repo]
  
  let appBar = MDCAppBar()
  
  private lazy var header: UserHeader = {
    let header = UserHeader(user: self.user)
    
    return header
  }()
  
  private lazy var table: RepoTableView = {
    let table = RepoTableView(repos: self.repos)
    table.delegate = self
    
    return table
  }()
  
  init(user:User, repos:[Repo]) {
    self.user = user
    self.repos = repos;
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    self.title = user.login
    self.configureViews()
  }
  
  private func configureViews() {
    self.view.addSubview(self.table)
    self.view.addSubview(self.header)
    constrain(self.header, self.table) { (header, table) in
      header.top == header.superview!.top + 65
      header.leading == header.superview!.leading
      header.trailing == header.superview!.trailing
      
      table.top == header.bottom + 15
      table.leading == table.superview!.leading
      table.trailing == table.superview!.trailing
      table.bottom == table.superview!.bottom
    }
  }
}

extension UserViewController: RepoTableViewDelegate {
  func didSelect(repo:Repo) {
    let viewController = RepoDetailViewController(repo: repo)
    self.navigationController?.pushViewController(viewController, animated: true)
  }
}
