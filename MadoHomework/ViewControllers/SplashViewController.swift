//
//  SplashViewController.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/20/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit
import Hero
import SwiftyGif
import Cartography

class SplashViewController: UIViewController {
  private lazy var logo: UIImageView = {
    let gifManager = SwiftyGifManager(memoryLimit:20)
    let gif = UIImage(gifName: "logo.gif")
    let imageview = UIImageView(gifImage: gif, manager: gifManager)
    imageview.contentMode = .scaleAspectFit
    
    return imageview
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configureView()
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
      self.transition()
    })
  }
  
  func configureView() {
    self.view.backgroundColor = .white
    self.view.addSubview(self.logo)
    constrain(self.logo) { (logo) in
      logo.center == logo.superview!.center
      logo.width == 200
    }
  }
  
  func transition() {
    let usernameViewController = UsernameViewController.navWrapped()
    usernameViewController.isHeroEnabled = true
    usernameViewController.heroModalAnimationType = .fade
    self.hero_replaceViewController(with: usernameViewController)
  }
}
