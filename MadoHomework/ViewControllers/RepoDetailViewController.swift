//
//  UserDetailViewController.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/22/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit

class RepoDetailViewController: UIViewController {
  let repo:Repo
  
  @IBOutlet weak var nameLabel:UILabel!
  @IBOutlet weak var descriptionLabel:UILabel!
  @IBOutlet weak var forksLabel:UILabel!
  @IBOutlet weak var starsLabel:UILabel!
  @IBOutlet weak var urlLabel:UILabel!
  
  
  init(repo:Repo) {
    self.repo = repo
    super.init(nibName: "RepoDetail", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configure()
  }
  
  private func configure() {
    self.nameLabel.text = self.repo.name
    self.descriptionLabel.text = self.repo.description
    self.forksLabel.text = "Forks: \(self.repo.forks)"
    self.starsLabel.text = "Stars: \(self.repo.stargazers_count)"
    self.urlLabel.text = self.repo.clone_url
  }
}
