//
//  RepoTableView.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit
import Cartography

protocol RepoTableViewDelegate: class {
  func didSelect(repo:Repo)
}

class RepoTableView: UIView {
  weak var delegate:RepoTableViewDelegate?
  
  let repos:[Repo]
  
  private (set) lazy var tableview: UITableView = {
    let tableview = UITableView()
    tableview.dataSource = self
    tableview.delegate = self
    tableview.register(RepoCell.self, forCellReuseIdentifier: "repoCellIdentifier")
    tableview.rowHeight = UITableViewAutomaticDimension
    tableview.estimatedRowHeight = 300
    tableview.tableFooterView = UIView()
    
    return tableview
  }()
  
  init(repos:[Repo]) {
    self.repos = repos
    super.init(frame: CGRect.zero)
    self.configureViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    self.addSubview(self.tableview)
    constrain(self.tableview) { (table) in
      table.edges == table.superview!.edges
    }
  }
}

extension RepoTableView: UITableViewDataSource, UITableViewDelegate {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.repos.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableview.dequeueReusableCell(withIdentifier: "repoCellIdentifier", for: indexPath) as! RepoCell
    let repo = self.repos[indexPath.row]
    cell.configure(repo: repo)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let repo = self.repos[indexPath.row]
    tableView.deselectRow(at: indexPath, animated: true)
    self.delegate?.didSelect(repo: repo)
  }
}
