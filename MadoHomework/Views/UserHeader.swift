//
//  UserHeader.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit
import Cartography
import SDWebImage

class UserHeader: UIView {
  let user: User
  
  private lazy var avatarImageView: UIImageView = {
    let imageview = UIImageView()
    imageview.sd_setImage(with: self.user.avatar)
    imageview.layer.cornerRadius = 25
    imageview.layer.masksToBounds = true
    
    return imageview
  }()
  
  private lazy var usernameLabel: UILabel = {
    let label = UILabel()
    label.text = self.user.login
    label.font = UIFont.systemFont(ofSize: 14.0)
    
    return label
  }()
  
  private lazy var nameLabel: UILabel = {
    let label = UILabel()
    label.text = self.user.name
    label.font = UIFont.systemFont(ofSize: 12.0)
    
    return label
  }()
  
  private lazy var dateLabel: UILabel = {
    let label = UILabel()
    label.text = self.user.created_at
    label.font = UIFont.systemFont(ofSize: 10.0)
    
    return label
  }()
  
  init(user:User) {
    self.user = user
    super.init(frame: CGRect.zero)
    self.configureView()
    self.backgroundColor = UIColor.lightGray
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureView() {
    self.addSubview(self.avatarImageView)
    self.addSubview(self.usernameLabel)
    self.addSubview(self.nameLabel)
    self.addSubview(self.dateLabel)
    
    constrain(self.avatarImageView, self.usernameLabel, self.nameLabel, self.dateLabel) { (avatar, username, name, date) in
      avatar.width == avatar.height
      avatar.width == 50
      avatar.leading == avatar.superview!.leading + 15
      avatar.top == avatar.superview!.top + 30
      avatar.bottom == avatar.superview!.bottom - 30
      
      username.top == avatar.top + 5
      username.leading == avatar.trailing + 15
      
      date.centerY == username.centerY
      date.leading == username.trailing + 10
      
      name.top == username.bottom + 5
      name.leading == username.leading
    }
  }
}
