//
//  RepoCell.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import UIKit
import Cartography

class RepoCell: UITableViewCell {
  var repo: Repo?
  
  private lazy var nameLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
    
    return label
  }()
  
  private lazy var descriptionLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    
    return label
  }()
  
  private lazy var forksLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 12.0)
    return label
  }()
  
  private lazy var starsLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 12.0)
    
    return label
  }()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.configureView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureView() {
    self.contentView.addSubview(self.nameLabel)
    self.contentView.addSubview(self.descriptionLabel)
    self.contentView.addSubview(self.forksLabel)
    self.contentView.addSubview(self.starsLabel)
    constrain(self.nameLabel, self.descriptionLabel, self.forksLabel, self.starsLabel) { (name, desc, fork, star) in
      name.top == name.superview!.top + 15
      name.leading == name.superview!.leading + 15
      name.trailing == name.superview!.trailing - 15
      
      desc.top == name.bottom + 15
      desc.leading == name.leading
      desc.trailing == name.trailing
      
      fork.top == desc.bottom + 15
      fork.leading == desc.leading
      
      star.leading == fork.trailing + 15
      star.top == fork.top
      
      fork.bottom == fork.superview!.bottom - 15
    }
  }
  
  func configure(repo:Repo) {
    self.repo = repo
    self.nameLabel.text = repo.name
    self.descriptionLabel.text = repo.description
    self.forksLabel.text = "Forks: \(repo.forks)"
    self.starsLabel.text = "Stars: \(repo.stargazers_count)"
  }
}
