//
//  Api.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import Alamofire
import PromiseKit
import SwiftyJSON

public class Response {
  public let statusCode : Int
  public let json : JSON
  
  init(statusCode : Int, json : JSON) {
    self.statusCode = statusCode
    self.json = json
  }
}

public class ErrorResponse : Response, Error {
  public let title : String
  public let message : String
  
  init(statusCode : Int, json : JSON, title : String?, message : String?) {
    self.title = title ?? "Error"
    self.message = message ?? "There was an error processing your request."
    
    super.init(statusCode: statusCode, json: json)
  }
}

public class Api {
  let baseURL = URL(string: "https://api.github.com/")!
  var name: String?
  
  public init(name : String? = nil) {
    self.name = name
  }
  
  public func request(method : HTTPMethod, path : String, params : [String : AnyObject]?) -> Promise<Response> {
    let (p, f, r) = Promise<Response>.pending()
    let url = self.baseURL.appendingPathComponent(path)
    let encoding = self.paramEncodingForMethod(method: method)
    
    Alamofire.request(url, method: method, parameters: params, encoding: encoding).responseJSON { response in
      print("response: \(response)")
      let (error, response) = self.process(response: response)
      if error != nil {
        switch error {
        case let e as ErrorResponse:
            f(e)
          return
        default:
          r(error!)
          return
        }
      }
      
      f(response!)
    }
    
    return p
  }
  
  private func process(response : DataResponse<Any>) -> (Error?, Response?) {
    let statusCode = response.response?.statusCode ?? 999
    let value = response.result.value
    let json = JSON(value ?? [:])
    let errorTitle = json["error"]["title"].string
    let errorMessage = json["error"]["message"].string
    
    switch response.response?.statusCode ?? 999 {
    case 200...299:
      return (nil, Response(statusCode: statusCode, json: json))
    default:
      return (ErrorResponse(statusCode: statusCode, json: json, title: errorTitle, message: errorMessage), nil)
    }
  }
  
  private func paramEncodingForMethod(method : HTTPMethod) -> ParameterEncoding {
    switch method {
    case .put, .post, .patch:
      return JSONEncoding.default
    default:
      return URLEncoding.default
    }
  }
}
