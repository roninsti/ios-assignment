//
//  User.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftDate
import SwiftyJSON

struct User {
  var login: String {
    return self.json["login"].stringValue
  }
  
  var name: String {
    return self.json["name"].stringValue
  }
  
  var created_at: String {
    guard let date = DateInRegion(string: self.json["created_at"].stringValue, format: .iso8601(options: .withInternetDateTime)) else {
      return "Join date unknown"
    }
    return "joined: " + date.string(dateStyle: .long, timeStyle: .none)
  }
  
  var avatar: URL? {
    return URL(string: self.json["avatar_url"].stringValue)
  }
  
  let json:JSON
  
  init?(json:JSON) {
    guard let _ = json["login"].string,
          let _ = json["name"].string,
          let _ = json["created_at"].string,
          let _ = json["avatar_url"].string else {
            return nil
          }
    self.json = json
  }
  
  static func get(username:String) -> Promise<(User?, ErrorResponse?)> {
    return Api.init(name: "User").request(method: .get, path: "users/\(username)", params: nil).then{ response -> (User?, ErrorResponse?) in
      if let r = response as? ErrorResponse {
        return (nil, r);
      }
      
      guard let user = User(json: response.json) else {
        throw ErrorResponse(statusCode: 0, json: JSON(), title: "User Error", message: "User could not be generated, invalid JSON")
      }
      
      return (user, nil)
    }
  }
}
