//
//  Repo.swift
//  MadoHomework
//
//  Created by Craig Cronin on 12/21/17.
//  Copyright © 2017 Mado Labs. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyJSON

struct Repo {
  var name: String {
    return self.json["name"].stringValue
  }
  
  var description: String {
    return self.json["description"].stringValue
  }
  
  var forks: NSNumber {
    return self.json["forks_count"].numberValue
  }
  
  var stargazers_count: NSNumber {
    return self.json["stargazers_count"].numberValue
  }
  
  var clone_url: String {
    return self.json["clone_url"].stringValue
  }
  
  let json:JSON
  
  init?(json:JSON) {
    guard let _ = json["name"].string,
          let _ = json["description"].string,
          let _ = json["forks_count"].number,
          let _ = json["stargazers_count"].number,
          let _ = json["clone_url"].string else {
            return nil
          }
    self.json = json
  }
  
  static func get(username: String) -> Promise<([Repo]?, ErrorResponse?)> {
    return Api(name: "Repo").request(method: .get, path: "users/\(username)/repos", params: nil).then{ response -> ([Repo]?, ErrorResponse?) in
      if let r = response as? ErrorResponse {
        return (nil, r);
      }
      
      guard let reposJson = response.json.array else {
        throw NSError(domain: "Repo", code: 0, userInfo: [NSLocalizedDescriptionKey:"Repos could not be generated, invalid JSON"])
      }
      
      var repos = [Repo]()
      reposJson.forEach{ repoJson in
        if let repo = Repo(json: repoJson) {
          repos.append(repo)
        }
      }
      
      return (repos, nil)
    }
  }
}
